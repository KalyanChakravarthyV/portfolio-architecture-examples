= Cloud adoption
Eric D. Schabell @eschabell, Iain Boyle @iainboy
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


This architecture covers cloud adoption. As enterprises adopt to public and/or private clouds, it is important to
provide automation to manage and scale server deployments and to provide the capability to transition servers between data centers
and cloud providers. This provides flexibility and portability.

Use case: Accelerating cloud adoption with effective automation for deploying and managing workloads across multiple cloud
infrastructures according to performance, security, compliance, and cost requirements. 

Open the diagrams below directly in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your content.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/cloud-adoption.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/cloud-adoption.drawio?inline=false[[Download Diagram]]
--
--
image:intro-marketectures/cloud-adoption-marketing-slide.png[750,700]
--

--
image:logical-diagrams/cloud-adoption-ld.png[350, 300]
image:schematic-diagrams/cloud-adoption-network-sd.png[350, 300]
image:schematic-diagrams/cloud-adoption-data-sd.png[350, 300]
--

--
image:detail-diagrams/cloud-adoption-smart-management.png[250, 200]
image:detail-diagrams/cloud-adoption-automation.png[250, 200]
image:detail-diagrams/cloud-adoption-scm.png[250, 200]
image:detail-diagrams/cloud-adoption-image-store.png[250, 200]
--

